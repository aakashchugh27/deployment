**How to deploy ML model on Heroku?**

Once the model is built and your app is ready, be it in flask or django or streamlit. It is time to host it somewhere online and we are going to use Heroku for that
Make sure that you maintain a structure like this


```
├── app.py
├── requirements.txt
├── setup.sh
└── Procfile
```


**Create an account on [Heroku](https://www.heroku.com/)**

Hosting an app is for free on Herou. However, the app will be hosted on heroku domain. You can add a custom domain if you already own one.
You can read in detail about changing domain names [here](https://devcenter.heroku.com/articles/custom-domains).

**Set up Heroku**

If you already have git in your system then skip step 1.

**Step 1: Install Git**

To set up heroku we need to install heroku CLI which requires Git. If you don't have git installed then follow the steps mentioned [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)


**Step 2: Install Heroku CLI**

In this step you'll install Heroku CLI to manage, scale and run your applications.

According to your OS download and finish the set up from [here](https://devcenter.heroku.com/articles/getting-started-with-python#set-up)

**Host your app**

**Step 1: Initialise git for your project, add the changes and perform a commit**

Now open your cmd and navigate to the project/app folder using cd and type the following commands.

1.  git init
2.  git add .
3.  git commit -m "first commit"

**Step 2: Login to heroku**

Now to host the app on heroku you need to login, then create the app and push your commit to heroku. Open your cmd and navigate to the same project folder and type the following commands

1.  heroku login
2.  heroku apps:create nameoftheapp
3.  git push heroku master

Now the app is deployed and it will automatically open the app in your default browser.





